# Setup Instructions for CNN Gender Classification

## Install `conda`

1. [download](https://repo.anaconda.com/miniconda/Miniconda3-latest-Linux-x86_64.sh) linux installer for Python 3.7
2. in terminal run: `bash Miniconda3-latest-Linux-x86_64.sh`
3. follow the prompts on the installer screens
   - If you are unsure about any setting, accept the defaults. You can change them later.
4. To make the changes take effect, close and then re-open your terminal window.
5. Test your installation. In your terminal window or Anaconda Prompt, run the command `conda list`. A list of installed packages appears if it has been installed correctly.

## Manually Create the Required Folder Structure

1. download the [dataset](http://vis-www.cs.umass.edu/lfw/lfw-deepfunneled.tgz)
2. extract the archive: `tar -xvf ~/Downloads/lfw-deepfunneled.tgz`
3. download the project from [Google Drive](https://drive.google.com/file/d/1d4wky1dKDey__EYr5MtpPOidFlxkGWrJ/view?usp=sharing)
   - contains the results of ***all the steps taken in data processing***:
      1. all images in one folder
      2. images split to folder by gender
      3. image data converted to numpy arrays
4. extract the archive: `tar -xvf cnn-gender-classification.tar.gz`
5. move the extracted archive to the project directory: `mv ~/Downloads/lfw-deepfunneled /path/to/project/`
   - `/path/to/project/lfw-deepfunneled` - contains the extracted data set
6. switch to the folder containing the project - `cd /path/to/project/`
7. create the following folders in they don't exist:
   - `mkdir data`
   - `mkdir -p ./lwf_gender_labeled_data/females`
   - `mkdir -p ./lwf_gender_labeled_data/males`
   - `mkdir ./data_arrays`
   - `mkdir ./cnn_models`
   - `mkdir ./outputs`

## Project Overview

The project performs uses convolutional neural network for the gender classification based on face image [200 x 200]

### CNN Structure

- 3 convolution layers
- each layer contains 32 7x7 filters
- each layer also contains a max pooling operation with a reduction in size by half for both width and height and a rectified linear unit for activation
- the output of last layer is flattened to provide the input layer of the fully connected multi-layer perceptron with 512 hidden neurons and one output neuron

### Dataset

- The data set contains more than 13,000 images of faces collected from    the web.  
- Each face has been labeled with the name of the person pictured.  
- 1680 of the people pictured have two or more distinct photos in the data set.  
- The only constraint on these faces is that they were detected by the Viola-Jones face detector.
- Link to dataset: [Labeled Faces in the Wild](http://vis-www.cs.umass.edu/lfw/)

### Code Overview

- the code contains the 3 functions for data preparation:
  - `move_images_to_one_folder()`
  - `split_by_gender()`
  - `save_images_to_array()`
- the `cnn()` creates the neural network and does all the processing
- the `main()` serves as a wrapper around the previous functions and contains all the required parameters required for processing.

### Folder Structure Overview

- `data` - contains all the images in one folder after `move_images_to_one_folder()` has run
- `lwf_gender_labeled_data` - contains the files of male/female file names
  - `lwf_gender_labeled_data/males` - contains images of males after `split_by_gender()` has run
  - `lwf_gender_labeled_data/females`  - contains images of females after `split_by_gender()` has run
- `data_arrays` - contains male/female image data in numpy arrays
- `outputs` - contains data classes in `classes.txt`
  
## Running the Project

1. create conda venv: `conda env create -f /path/to/project/cnn_conda_venv.yml`
2. activate conda env: `conda activate gender-cnn`
3. run the `cnn.py`: `python /path/to/project/cnn.py`

### Issues

- the folder creation in data prep functions may not work properly to avoid issues please see [here](#manually-create-the-required-folder-structure) to manually create required folder structure

## Results

### Computational Time

- Elapsed Time for Training/Testing: 5981.776989221573 s

### Confusion Matrix

![conf matrix](outputs/confusion_matrix.png)
